@extends('layouts.app')

@section('title', 'Startseite')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Startseite</div>

                <div class="card-body">
                  Willkommen auf der Startseite
                    <span class="btn btn-primary"><i class="fas fa-plus"></i>Hallo</span>
                  <span class></span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
